const toggle = n => document.getElementById('supp'+n).classList.toggle('show');

// use parentNode because h2 is sticky and could be displaced
// (however, if not wrapped in section/div prob at 0, so fall back to h2)
const reback = () =>
   document.querySelectorAll('h2').forEach(el =>
      el.style.backgroundPosition = '0 -'
         + (el.parentNode.offsetTop || el.offsetTop) + 'px');

document.addEventListener('DOMContentLoaded', () => {
   reback();
   document.querySelectorAll('button[data-toggle]').forEach(el =>
      el.addEventListener('click', ev =>
         toggle(ev.currentTarget.getAttribute('data-toggle'))));
   });
window.addEventListener('resize', reback);

