syn keyword ggaTodo			TODO TODO: TBD TBD: FIXME FIXME: XXX XXX: NOTE NOTE: contained
syn match  ggaComment          "#.*" contains=@Spell,ggaTodo extend
"syn match  ggaTag              "<[^>]*>"
syn region ggaTag start="<" end=">"

syn region personBlock start="^\d\+  \|^\d\+=\d\+" end="^$" contains=firstLine,ggaEvents,@ggaItem
syn match firstLine "^\d.*$" contained contains=ggaName,ggaRoct,ggaEqual
syn match ggaEvents "^\t.*$" contained contains=ggaEvent,ggaComment
syn match ggaDate ".*\d\d\d\d.*" contained
syn match ggaName "  .*:" contained
syn match ggaEvent "^\t[^ #]\+" contained
syn match ggaRoct "^\d\+=\?\d*" contained
syn match ggaEqual "=" contained

syn region ggaItem contained start="  " end="  " contains=ggaDate

hi def link ggaComment         Comment
hi def link ggaStatement		Statement
hi def link ggaLine           Type
hi def link ggaData			   Identifier
hi def link ggaColon			   Special
hi def link ggaTodo           Todo
hi def link ggaTag            Function

"hi def link personBlock       Todo
hi def link firstLine       String
"hi def link ggaEvents       Identifier
hi def link ggaEvent       Identifier
hi def link ggaRoct        Statement
"hi def link ggaDate       Statement
hi def link ggaName       Type
hi def link ggaEqual       Comment

"no effect
"hi def link ggaItem   Todo

