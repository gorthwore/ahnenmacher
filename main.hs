module Main (loadMe, main) where

--+
import BasePrelude

import Data.ByteString.Lazy as BL
import Data.ByteString.Builder

import Heist.Compiled

import Control.DeepSeq

import Ahnen.Base
import Ahnen.Ahnen
import Ahnen.Gedcom
import Ahnen.Heist
import Ahnen.Parse

-- import qualified Data.Map.Lazy as M


--  For ghci testing.
loadMe :: IO Ahnen
''  = do
   s <- readFile "/home/galen/hg/web/anc/me.gga"
   let (_, x, _) = readAhnen s in return x

toHtml :: Bool -> AhnenPlus -> IO ()
''  v ahp = do
   let logm = when v . traceLogM
   --  does not yet force coverage or allthrus, since those are hard to constrain
   let AhnenPlus { .. } = ahp in
      rnf (length $ show $ people _ahnen, linkMap _ahnen, _multis, _ilinks, _invLinks)
         `seq` return ()
   logm "Rendering."
   hs <- topHeist
   putStrLn "<!DOCTYPE html>"
   BL.putStr . toLazyByteString =<< do
      runMyMonad ahp $ fst $ fromJust $ renderTemplate hs "top"
   logm "Done."

main = do
   args <- getArgs
   let verbose = any (=="-v") args
   let logm = when verbose . traceLogM
   logm "Loading."
   (pre, ahnen, post) <- readAhnen <$> hGetContents stdin
   logm "Parsing tree."
   checkAhnen ahnen `seq` return ()
   logm "Computing data."
   let ahp = (stuffAhnen ahnen) { _extras = toDyn (pre, post) }
   if any (=="-g") args
   then putStr $ ahnenToGedcom ahnen
   else toHtml verbose ahp

