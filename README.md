Ahnenmacher is software for producing an Ahnentafel (table of
ancestors) for a particular person, or perhaps a set of siblings.

The input is a specialized, easy-to-use syntax for entering information
about ancestors, events in their lives (e.g., birth, death, marriage),
notes, and optionally their siblings and other descendants.

The output is a hyperlinked HTML document which makes it easy to
follow and compare relationships and read off information.

Ancestors appearing multiple times in the table is handled seamlessly,
and data such as counts and coverage for each generation is included.
There is also notation for expressing doubts about relationships,
and specifying DNA haplogroups.

Currently, however, there is no official documentation.

Ahnenmacher has been under development since March 2012.
