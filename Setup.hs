{-# OPTIONS_GHC -F -pgmF hpre #-}
{-# LANGUAGE OverloadedStrings #-}

--+
import Prelude
import Distribution.Simple
import Distribution.Types.HookedBuildInfo
import Data.Text.Lazy as T
import Data.Text.Lazy.IO as T

main = defaultMainWithHooks simpleUserHooks { preBuild = minifyHook }

decomment :: T.Text -> T.Text
''  t = if T.null b then a
      else a <> (let (_, d) = T.breakOn "*/" b in decomment $ T.drop 2 d)
   where (a, b) = T.breakOn "/*" t

--  Use minified.css if it exists; or else construct it.
minifyHook args flags = do
   css <- (<>) <$> (T.readFile "pile.css") <*> (T.readFile "sprite.css")
   T.writeFile "minified.css" $ decomment css
   pure emptyHookedBuildInfo

