module Ahnen.Heist (runMyMonad, topHeist) where

--+
import BasePrelude

import Heist
import Heist.Compiled
import Heist.Compiled.LowLevel
import Heist.Interpreted as I
import Heist.Internal.Types
import Heist.Splices (ifCSplice)
import Lens.Micro
import Text.Heredoc hiding (str)
import Text.XmlHtml hiding (isComment)
import Data.Text as T
import Data.ByteString.Builder

import Ahnen.Base
import Ahnen.Html
import Ahnen.Utils (rootGentag, unforwards)
import Data.Map.Syntax
import Data.Time
import Data.Time.Format.ISO8601 (iso8601Show)
import Data.HashMap.Strict as HM
import Data.Map (Map), as M
import Data.Set as S
import Data.MultiMap as MM

-- import Control.Monad.Trans
-- import Control.Monad.Trans.Reader
import Control.Monad.Reader
import Control.Monad.RWS.Strict
import Control.Monad.Trans.Maybe
import Fmt hiding (Builder)


newtype MyMonad a = MyMonad (RWST AhnenPlus () (HeistState Identity) IO a)
   deriving newtype (Functor, Applicative, Monad, MonadIO)

getAP :: MyMonad AhnenPlus
''  = MyMonad ask
getAPS :: (AhnenPlus -> a) -> MyMonad a
''  = MyMonad . asks
getIHS :: MyMonad (HeistState Identity)
''  = MyMonad get
putIHS :: (HeistState Identity) -> MyMonad ()
''  = MyMonad . put

runMyMonad :: AhnenPlus -> MyMonad a -> IO a
''  ahp (MyMonad act) = do
   hs <- liftIO $ fmap (ecatch "IHeist") $ initHeist baseConf
   fst <$> evalRWST act ahp hs

-- not using anymore (well, never used, but was going to)
{-
{-# NOINLINE simpleHS #-}
simpleHS :: HeistState Identity
''  = let Right x = unsafePerformIO $ initHeist baseConf in x
-}

-- only used for interp splices as of now
baseConf = emptyHeistConfig & hcNamespace .~ "" & hcInterpretedSplices .~ markupSplices

eventMap :: Map String String
''  = M.fromList $
	("b", "&lowast;") :
	("d", "&#x271D;") :
	("bd", "&perp;") :
	("m", "&#x26AD;") :
	("cw", "×") :
	("bp", "β") :
	("mv", "→") :
	("r", "&#8962;") :
	("kt", "&#9816;") :
	[]

spliceNodes :: Monad n => Splices (I.Splice n) -> Template -> HeistT n n Template
''  spls = localHS (I.bindSplices spls) . I.runNodeList

mkDoc :: Template -> DocumentFile
''  tpl = DocumentFile { dfDoc = HtmlDocument {
            docEncoding = UTF8, docContent = tpl, docType = Nothing }
         , dfFile = Nothing }

mkHeist :: Monad n => TemplateLocation -> Splices (I.Splice IO)
      -> Splices (Splice n) -> IO (HeistState n)
''  tpls loadtimes compileds = do
   let sconf = mempty
         & scTemplateLocations .~ pure tpls
         & scCompiledSplices .~ compileds
         & scLoadTimeSplices .~ defaultLoadTimeSplices <> loadtimes
   fmap (ecatch "mkHeist") $ initHeist $ emptyHeistConfig
      & hcSpliceConfig .~ sconf
      -- & hcNamespace .~ ""
      & hcCompiledTemplateFilter .~ (== ["top"])
      & hcErrorNotBound .~ True -- False -- temporary


--  Interpreted splices for use with user text.
markupSplices :: Monad n => Splices (I.Splice n)
''  = do
	let surround x = (:[]) <$> x <$> I.runChildren
	let surSpan x = surround $ Element "span" [("class", x)]

	"para" ## surround $ Element "p" [("class", "auto_p")]
	"rem" ## pure mempty

	-- replace soon
	"dv" ## pure [TextNode "\9902"]
	"header" ## do
		~(Just name) <- getAttribute "name" <$> getParamNode
		pure [Element "title" [] [TextNode $ "Ancestry of " <> name]]

	-- basic mark-up
	"star1"       ## surround $ Element "b" []
	"slash1"      ## surSpan "sc"
	"underscore1" ## surround $ Element "i" []
	"at1"         ## surSpan "ats"
	"at2"         ## surSpan "at2"
	"listitem"    ## surround $ Element "li" []
	"caret2"      ## surround $ Element "a" [("class", "cite"), ("href", "#biblio")]
	-- not as basic
	"caret1" ## do
      -- traceM . show =<< getParamNode
		~(Element _ _ [node]) <- getParamNode
      let ref = case node of
            TextNode num    -> num
            Element tag _ _ -> "${" <> tag <> "}"
            _               -> error "invalid caret1 contents"
		pure [Element "a" [("href", "#" <> ref), ("class", "pl")] [TextNode "°", node]]

	-- elements, may move
	"img1" ## do
		~(Element _ ats _) <- getParamNode
		pure [flip (Element "img") [] do
			pair@(k, v) <- ats
			pure case k of
				"src" -> ("src", "/anc/img/" <> v)
				"class1" -> ("class", v)
				_     -> pair ]
	"alink" ## do
		~(Element _ [("url", url)] []) <- getParamNode
		pure [Element "a" [("href", url)] [TextNode url]]
	"ax" ## do
		~(Element _ [("h", url)] inner) <- getParamNode
		pure [Element "a" [("href", url), ("class", "xr")] inner]

	-- a primitive version with no args or the like
	"macro" ## do
		me <- getParamNode
		let Just name = getAttribute "name" me
		let childs = childNodes me
		modifyHS $ I.bindSplice name $ I.runNodeList childs
		pure mempty

	"drafthead" ## do
		let UTCTime { .. } = unsafePerformIO getCurrentTime
		let TimeOfDay hours _ _ = timeToTimeOfDay utctDayTime
      spliceNodes
         do mapV I.textSplice do
               "date" ## T.pack $ iso8601Show utctDay
					"day-part" ## T.singleton $ "⊖⊙⊕" !! (fromIntegral hours `div` 8)
         do hx [here|<div style="float:right" :>
            <i>Draft</i> <time datetime="${date}"><date/> <day-part/></> UTC|]

   -- dubious
   "double" ## do
      ~(Element _ _ n) <- getParamNode
      pure $ n <> n

   "icon" ## do
      ~(Element _ ats _) <- getParamNode
      let cls = maybe "" (" " <>) $ lookup "class" ats
      pure [Element "span" [
         if k=="i" then ("class", "icon i-" <> v <> cls) else p
            | p@(k, v) <- ats]
         []]

--  Bool indicates whether to force a paragraph interpretation.
--  (Necessary because xmlhtml drops initial spaces.)
userContentSplice :: Bool -> RuntimeSplice MyMonad String -> Splice MyMonad
''  ifp rts = pure $ yieldRuntime do
   hs <- lift getIHS
   str <- rts
   let proc = if ifp then hxm_p else hxm
   let (tpl, hs') = runIdentity
         $ runHeistT (I.runNodeList $ proc str) undefined hs
   lift $ putIHS hs'
   pure $ htmlNodeSplice id tpl

-- TODO may not need a top template at all
topHeist :: IO (HeistState MyMonad)
''  = mkHeist (pure $ pure $ fmap mkDoc $ tpls) markupSplices spls where
   tpls = HM.fromList $
      (["top"], topTpl) :
      []
   extras :: MyMonad (String, String)
   ''  = flip fromDyn ("", "") <$> getAPS _extras
   spls = do
      "styles" ## pure $ yieldPure $
         "<style type='text/css'>"
            <> [there|minified.css|] <> "</style>"
      "scripts" ## pure $ yieldPure $
         "<script type='text/javascript'>" <> [there|pile.js|] <> "</script>"
      "generation" ## generationSplice
      "pre" ## get fst
      "post" ## get snd
      where get x = userContentSplice False $ fmap x $ lift extras

headFor :: Int -> Builder
''  0 = "Focus"
''  1 = "Parents"
''  g = tag <> "parents" where
   tag | g==1 = ""
       |      = pf <> "Grand" where
         pf | g==2 = ""
            |      = p2 <> "Great-" where
               p2 | g==3 = ""
                  |      = intDec (g-2) <> "×"

data GenBunch = GenBunch {
   , _gb_gen   :: !Int
   , _gb_gen'  :: !Int   -- relative to root
   , _gb_cvg   :: !Integer
   , _gb_root  :: !Sosa   -- repeated again and again?
   , _gb_ath   :: !Sosa
   , _gb_entrs :: [Entry]
   , _gb_tag   :: Maybe Builder
   }
generationSplice :: Splice MyMonad
''   = manyWithSplices runChildren spls $ lift $ getAPS mklist where
   spls :: Splices (RuntimeSplice MyMonad GenBunch -> Splice MyMonad)
   spls = do
      "gen" ## pureSplice $ intDec . _gb_gen
      -- kind of roundabout ways of seeing if first/last gen
      "up" ## genlink \ (GenBunch { _gb_gen = g }) -> do
         ah <- lift $ people <$> getAPS _ahnen
         (k, _) <- MaybeT $ pure $ M.lookupLT (2^g) ah
         guard $ k>0
         pure $ g - 1
      "dn" ## genlink \ (GenBunch { _gb_gen=g, _gb_entrs=e }) -> do
         guard $ case e of [Unknown _] -> False; _ -> True
         pure $ g + 1
      "coverage" ## pureSplice \ (GenBunch { _gb_gen', _gb_cvg = cvg }) -> do
         let genSize = pow2 _gb_gen'
         let cvgpct = showPct (100 * fromIntegral cvg / fromIntegral genSize)
         ""+|cvgpct|+"% ("+|showI cvg|+"/"+|showI genSize|+")"
      "allthru" ## \rt -> manyWithSplices runChildren
         do "atno" ## pureSplice fst
            "thru" ## ifCSplice snd
         do GenBunch { _gb_ath=ath, .. } <- rt
            pure do
               guard $ _gb_cvg > 0 && ath /= _gb_root
               Just (toRoctB ath, log2 ath + 1 <= _gb_gen)
      "entry"   ## manyWithSplices runChildren entrySplices . fmap _gb_entrs
      "gparent" ## pureSplice $ headFor . _gb_gen
      "if-sym"  ## manyWithSplices runChildren ("sym" ## pureSplice id) . fmap _gb_tag
   genlink :: (GenBunch -> MaybeT MyMonad Int) -> RuntimeSplice MyMonad GenBunch -> Splice MyMonad
   ''  f = mayDeferMap (lift . runMaybeT . f)
            $ withSplices runChildren $ "g" ## pureSplice intDec
   mklist :: AhnenPlus -> [GenBunch]
   ''  (AhnenPlus { _ahnen = ahn, .. }) = do
         let cvg = _coverage M.! onlyOn
         (g, cvg1, ath, ent) <- zip4
            [_firstgen ..]
            (drop (_firstgen - onlyDep) cvg)
            (drop _firstgen _allthrus)
            _entries
         let g' = g - onlyDep
         pure $ GenBunch g g' cvg1 onlyOn ath ent
            do rootGentag ahn g
      where
         onlyOn = rootSosa ahn
         onlyDep = log2 onlyOn

data BioBunch = BioBunch {
   , _bb_person :: !Person
   , _bb_sex    :: !Sex
   , _bb_backs  :: ![Sosa]
   , _bb_doubts :: !DoubtEntry
   }
mkBioBunch :: Sosa -> MyMonad BioBunch
''  sosa = do
   AhnenPlus { .. } <- getAP
   let ppl = people _ahnen
   let sex = if sosa>1 then sosaSex sosa else _rootSex
   let backs = sort $ filter (not . flip M.member (people _ahnen)) $ unforwards _invLinks sosa
   pure $ BioBunch (ppl M.! sosa) sex backs (_doubts M.! sosa)

--  Ported over directly from old code, probably needs optimization.
capline :: Maybe String -> Int -> String
''  cap n = let
   rcap = maybe "" reverse cap
   (sfx, rest) = span (not . isSpace) rcap
   (cap', fmt) = case sfx of
      _:_ | last sfx == ':' -> (reverse rest, tail $ reverse sfx)
      _  -> (reverse rcap, "#")
   mbnull _ "" = ""; mbnull f s = f s
   in
   (mbnull (' ':) cap') ++ (mbnull (\s -> " (" ++ s ++ ")") $
      let sn = show n
      in case fmt
         of "" -> ""; "+" -> sn ++ "+"; ".." -> sn ++ "..";
            "+-" -> sn ++ "&plusmn;"; "~" -> "&sim;" ++ sn;
            "~#+-" -> "&tilde;" ++ sn ++ "&plusmn;"
            _ -> concatMap (\x -> if x=='#' then show n else x:[]) fmt )

--  how it is always used
capline' (a, b) = capline a (length b)


--  a graveyard of failed abstractions

data Badger = forall f a. Foldable f => Badger
   !(BioBunch -> MyMonad (f a))
   !(Splices (RuntimeSplice MyMonad a -> Splice MyMonad))

data DnaBunch = DnaBunch !Sex !Sex !Haplogroup

mkBadge :: Badger -> RuntimeSplice MyMonad BioBunch -> Splice MyMonad
''  (Badger f spls) = mkBadgeRaw f (withSplices runChildren spls)

mkBadge' a b = mkBadge (Badger a b)

mkBadgeRaw :: forall f a. Foldable f
   => (BioBunch -> MyMonad (f a))
   -> (RuntimeSplice MyMonad a -> Splice MyMonad)
   -> RuntimeSplice MyMonad BioBunch
   -> Splice MyMonad
''  f run n =
   deferMany (fmap ((yieldPure " \160 ") <>) <$> run) $ lift . f =<< n

badgesSplices :: Splices (RuntimeSplice MyMonad BioBunch -> Splice MyMonad)
''  = do
   "b-sex" ## mkBadge' (pure . Identity . _bb_sex) do
      "color" ## pureSplice $ fromSex "blue" "red"
      "symb" ## pureSplice $ fromSex "\9794" "\9792"
   "b-backs" ## mkBadge' (pure . (\x -> case _bb_backs x of [] -> Nothing; _ -> Just x)) do
      "count" ## bindLater \ (BioBunch { _bb_person }) -> do
         multis <- lift $ getAPS _multis
         pure $ showIB $ multis M.! sosa _bb_person
      "each" ## ((. fmap _bb_backs) . manyWithSplices runChildren) do
         "to" ## pureSplice toRoctB
         "link" ## bindLater \to -> do
            il <- lift $ getAPS _ilinks
            pure $ toRoctB $ fromJust $ S.lookupLE to il
   "b-parents" ## flip mkBadge' ("id" ## pureSplice $ toRoctB)
      do \ (BioBunch { _bb_person = Person { sosa } }) -> do
         Ahnen { people, linkMap } <- getAPS _ahnen
         pure $ listToMaybe
            $ filter (\x -> forward people linkMap x `M.member` people)
            $ let f = 2 * forward mempty linkMap sosa in [f,f+1]
   "b-kids" ## flip mkBadge' ("kid" ## pureSplice $ toRoctB)
      \ (BioBunch { _bb_person = Person { sosa }, _bb_backs }) -> do
         people <- people <$> getAPS _ahnen
         pure $ filter (`M.member` people) $ map half $ sosa : _bb_backs
   "b-dnaboxes" ## mkBadge'
      do \ (BioBunch { _bb_sex, _bb_person = Person { sosa } }) -> do
         dnas <- getAPS _dnaMap
         pure [ DnaBunch sex _bb_sex hg
            | (sex, sel) <- [(Male, yHg), (Female, mtHg)]
               , let info = dnas M.! sosa
               , hg <- maybeToList $ sel info ]
      do "color" ## pureSplice \ (DnaBunch mysex dnasex _) ->
            fromSex (fromSex "blue" "greyedblue" dnasex) "red" mysex
         "haplo" ## pureSplice $ textSplice \ (DnaBunch _ _ hg) -> hgMain hg
         "extra" ## pureSplice $ textSplice
               \ (DnaBunch _ _ hg) -> fromMaybe mempty $ hgExtra hg
   "b-x" ## mkBadge'
      do \ (BioBunch { _bb_person = Person { sosa } }) ->
            getAPS (xInfo . (M.! sosa) . _dnaMap)
      do mapV pureSplice do
         let showIf b s = if b then s else mempty -- may use more generally
         "max"   ## \x -> showIf (isMaxX x) "Maximum "
         "style" ## \x -> showIf (isMaxX x) "width:32px"
         "num"   ## intDec . xRecombs
         "s"     ## \x -> showIf (xRecombs x /= 1) "s"
   "b-doubts" ## \rts -> do
      dataP <- newEmptyPromise
      let spls = do
            "to" ## pure $ yieldRuntime $ toRoctB <$> fst <$> getPromise dataP
            "width" ## pure $ yieldRuntime
               $ (\w -> "width:"+|(round $ w*6 :: Int)|+"px")
               <$> snd <$> getPromise dataP
            "tform" ## pure $ yieldRuntime
               -- don't understand two-pixel correction...
               $ (\w -> "transform:matrix("+|w/2|+",0,0,1,"+|w*3-8|+",0)")
               <$> snd <$> getPromise dataP
      nodes <- filter isElement <$> childNodes <$> getParamNode
      unlinked : linked : faded : _
         <- traverse (withLocalSplices spls mempty . runNode) nodes
      mkBadgeRaw
         do \bb -> do
            let mysosa = sosa $ _bb_person bb
            let DoubtEntry fade full = _bb_doubts bb
            let fades = map (faded,) fade
            let fulls = catMaybes [
                  (,item) <$>
                  (if mysosa /= tgt
                     then if sz*3<1 then Nothing else Just linked
                     else Just unlinked)
                  | item@(tgt, sz) <- full ]
            pure $ filter notNull [fades, fulls]
         do bindLater $ fmap mconcat . traverse \ (ch, dt) -> do
               putPromise dataP dt
               codeGen ch
         do rts
   "b-supp" ## mkBadge' (pure . suppnotes . _bb_person) mempty
   "b-tags" ## \rts -> do
      -- this is so convoluted!
      --   TODO a promise or something for errors?
      paramP <- newEmptyPromise
      let spls = do
            "param" ## pure $ yieldRuntime $ stringUtf8
                        <$> fromMaybe (error "ptag needs parameter")
                        <$> getPromise paramP
            "no-p" ## pure $ yieldRuntime
                        $ maybe mempty (error "ptag takes no parameter")
                           <$> getPromise paramP
            -- not being used, just using dash now
            "defp" ## do
               ~(Element _ at rest) <- getParamNode
               ch <- runNodeList rest
               pure $ yieldRuntime do
                  p <- getPromise paramP
                  when (p == Nothing) $
                     putPromise paramP $ fmap T.unpack $ lookup "v" at
                  codeGen ch
            "dash" ## do
               ~(Element _ at rest) <- getParamNode
               ch <- runNodeList rest
               pure $ yieldRuntime do
                  p <- getPromise paramP
                  let v = T.unpack $ fromMaybe "" $ lookup "v" at
                  putPromise paramP $ Just
                     $ maybe v (\s -> v <> " — " <> scoreSpace s) p
                  codeGen ch
            "fglink" ## pure $ yieldRuntime do
               ("https://www.findagrave.com/memorial/" <>)
                  <$> stringUtf8 <$> fromMaybe (error "findagrave requires id in ptag")
                  <$> getPromise paramP
      list <- childNodes <$> getParamNode
      tmap' <- M.fromList <$> traverse (\ (a, b) -> (a,) <$> b) [
               (k, if null v then pure Nothing
                     else Just <$> withLocalSplices spls mempty (runNodeList v))
                  | Element k _ v <- list ]
      let tmap = M.insert "wk!" (tmap' M.! "wk-bang") tmap'
      -- traceLogM $ show $ M.keys tmap
      let getTag t = fromMaybe (error $ "unrecognized ptag " <> show t)
                        $ M.lookup t tmap
      mkBadgeRaw
         do \ (BioBunch { _bb_person = Person { tags } }) -> pure $ catMaybes
               [ (,p) <$> (getTag $ T.pack t)
                  | (t, p) <- MM.toList tags, head t /= '-' ]
         do \rts' -> do
            pure $ yieldRuntime do
               (chs, param) <- rts'
               putPromise paramP param
               codeGen chs
         do rts

entrySplices :: Splices (RuntimeSplice MyMonad Entry -> Splice MyMonad)
''   = do
   "nobio" ## pureSplice $ \case Bio _ -> ""; _ -> " nobio"
   "start" ## pureSplice $ toRoctB . entryStart
   "interval" ## pureSplice $ \case
      Bio x     -> dispRoctB x
      SameAs x  -> showRangeB x
      Unknown x -> showIntervalB x
   "unknown" ## mayDeferMap (pure . \case Unknown x -> Just x; _ -> Nothing) do
      withSplices runChildren do
         "ct" ## pureSplice $ showIB . \ (a, b) -> 1 + b - a
         "kid" ## kidOf fst
   "same-as" ##
      mayDeferMap (pure . \case SameAs x -> Just x; _ -> Nothing) $
      deferMap (\ (rt, gn) -> do
            Ahnen { linkMap, people } <- lift $ getAPS _ahnen
            let rt' = forward (if gn==0 then people else M.empty) linkMap rt
            pure (rt, (rt', gn))
         ) $
      withSplices runChildren do
         "o-id" ## bindLater \ (_, (rt', gn)) -> do
            toRoctB . fromJust . S.lookupLE (rt' `shiftL` gn)
               <$> lift (getAPS _ilinks)
         "o-range" ## pureSplice $ showRangeB . snd
         "known"   ## bindLater \ (_, (rt', gn)) -> do
            cvg <- lift $ getAPS _coverage
            pure $ showIB $ cvg M.! rt' !! gn
         "total"   ## pureSplice $ showIB . pow2 . snd . snd
         "kid"     ## kidOf \ (rt, (_, gn)) -> rt `shiftL` gn
   "person" ##
      mayDeferMap (lift . \case Bio x -> Just <$> mkBioBunch x; _ -> pure Nothing) $
      withSplices runChildren do
         "name" ## deferMap (pure . namePlus . _bb_person) $ userContentSplice False
         "badges" ## withSplices runChildren $ badgesSplices
         "events" ## deferMap (pure . events . _bb_person) eventsSplice
         "notes" ## mayDeferMap (pure . notes . _bb_person) content
         "suppnotes" ## mayDeferMap (pure . suppnotes . _bb_person) content
         "issues" ## deferMany items . (fmap $ kids . _bb_person)
         "sibs" ## mayDeferMap (pure . getsibs) items
   where
   content = withSplices runChildren $ "content" ## userContentSplice True
   items = withSplices runChildren do
      "items" ## deferMany (miniPersonSplice 6) . fmap snd
      "cap" ## userContentSplice False . fmap (("\8203" <>) . capline')
   --  Annoying plumbing to deal with main sibling.
   getsibs :: BioBunch -> Maybe (Maybe String, [Person])
   ''  (BioBunch { _bb_person = Person { sibs }, _bb_sex }) =
      ($ sibs) $ mapped . _2 . mapped %~ \pp ->
      -- TODO this is hacky, doing and undoing, can it be done directly?
      case namePlus pp of
         '*' : rest -> pp {
            , namePlus = "\\~" <> rest
            , tags = MM.insert "sex" (Just $ fromSex "m" "f" _bb_sex) $ tags pp }
         _          -> pp
   kidOf :: forall a. (a -> Sosa) -> RuntimeSplice MyMonad a -> Splice MyMonad
   ''  f = (. fmap f) $ mayDeferMap
      do \me -> S.lookupLE (half me) <$> lift (getAPS _ilinks)
      do withSplices runChildren do
            "id" ## pure . yieldRuntime . fmap toRoctB

eventsSplice :: RuntimeSplice MyMonad [Event] -> Splice MyMonad
''  = manyWithSplices
         (runNodeList $ hx [here|<span class=sp><h:event/></> |]) do
   --  TODO should parse each event separately
   "event" ## flip deferMap (userContentSplice False) \ (Event t v) -> do
      pure $ mconcat $ intersperse "\160 "
         $ fromMaybe (scoreSpace t) (M.lookup t eventMap) : v

--  Limited recursion depth, as I don't know how to unlimit in Heist.
miniPersonSplice :: Int -> RuntimeSplice MyMonad Person -> Splice MyMonad
''  dp = withSplices (runNodeList tpl) do
      "name" ## deferMap (pure . namePlus) $ userContentSplice False
      "events" ## deferMap (pure . events) eventsSplice
      "sex" ## deferMap (pure . sexmark . findTag "sex" . tags)
         $ withSplices runChildren sexspls
      "kids" ## deferMany items . fmap kids
   where
   tpl = hx [here|<div :>
      <div class=mini:><h:sex><span class="sex ${h:color}"><h:icon/></></>
         &nbsp;<b class=mini><h:name/></>
         <h:events/>
      <h:kids :>
         <div class=kid :>
            <h:cap:><div class=mini:><i:><h:content/>
            <h:items/>
      |]
   items | dp<1 = const $ pure $ yieldPure "recursion depth exceeded"
         |      = withSplices runChildren do
                     "items" ## deferMany (miniPersonSplice (dp-1)) . fmap snd
                     "cap" ## mayDeferMap (pure . fst)
                        (withSplices runChildren $ "content" ## pureSplice stringUtf8)
   sexspls = do
      "icon" ## pureSplice snd; "color" ## pureSplice fst
   sexmark = \case
      Just "m" -> ("blue", "♂")
      Just "f" -> ("red", "♀")
      Nothing  -> ("grey", "○")
      _        -> error "Invalid sex!"



topTpl :: Template
''  = hx [here|
<meta charset="utf-8"/>
<meta width="device-width,initial-scale=1" name=viewport/>
<h:styles/>
<h:scripts/>
<h:pre/>
<h:generation:><section id="G${h:gen}":>
   <h2 class=gen :>
      <div:>&nbsp;
         <div:><h:up><a href="#G${h:g}"/▴></>&nbsp;&nbsp;&nbsp;
         <div:><h:dn:><a href="#G${h:g}":>▾
      <a href="#G${h:gen}":><h:gparent/><h:if-sym> <at2><h:sym/></></>
   <div class=coverage:>Coverage: <h:coverage/><h:allthru>
      all <h:thru/through ><caret1><h:atno/></></>
   <h:entry:><div class="entry${h:nobio}" :>
      <a id="${h:start}">&#8203;</a><#:>
      <b class=top :>
         <a href="#${h:start}" class=quiet><h:interval/></><#:>
         <bind tag=greych :>
            <h:kid:>&nbsp; <a href='#${h:id}'><icon i=greychild title=Child/></>
         <h:unknown:>
            &nbsp; <at1><h:ct/> unknown</>
            <greych/>
         <h:same-as:>
            &nbsp; <at1/same as> &nbsp; <a href='#${h:o-id}' class=pl><h:o-range/></>
            &nbsp; <at1><h:known/> of <h:total/> known</>
            <greych/>
         <h:person :>
            &nbsp;<span class=name><h:name/></><#:>
      <h:person :>
         &nbsp;&nbsp;
         <h:badges :>
            <h:b-sex :>
               <span class="sex ${h:color}":><h:symb/>
            <h:b-backs :>
               <span class=multi:>×<h:count/><h:each :>
                  <a href="#${h:link}" title="=${h:to}"/&dArr;>
            <h:b-parents :>
               <a href="#${h:id}":><icon i=parents title=Parents/>
            <h:b-kids :>
               <a href="#${h:kid}":><icon i=child title=Child/>
            <h:b-tags :>
               <#:> NB: not in any particular order.
               <mayf:><no-p/>
                  <icon i=mayf title='Mayflower passenger'/>
               <rev:><no-p/>
                  <icon i=rev title='American Revolutionary'/>
               <gmigr:><no-p/>
                  <icon i=gmigr title='Great Migration Project'/>
               <wp :>
                  <a href="http://en.wikipedia.org/wiki/${h:param}"
                        title='Wikipedia article':>
                     <icon i=wp title=Wikipedia/>
               <fg :>
                  <a href="${h:fglink}" title='Find A Grave' :>
                     <icon i=fg title=FindAGrave/>
               <fgwk :>
                  <a href="${h:fglink}" title='(Find A Grave)' class=fade :>
                     <icon i=fg/>
               <wt :>
                  <a href="https://www.wikitree.com/wiki/${h:param}" title='WikiTree' :>
                     <icon i=wt/>
               <xtree :>
                  <span class=multi><a href="../me/#${h:param}"/&#8663;></>
               <wk :><h:dash v="Work on this person needed" :>
                  <icon i=work title="${h:param}"/>
               <wk-bang :><h:dash v="Needs serious work" :>
                  <icon i=work2 title="${h:param}"/>
               <qkr/><loy/><ok/><XXX/>
               <new/>
            <h:b-dnaboxes :>
               <span class="${h:color} dnabox" title="${h:extra}":><h:haplo/>
            <h:b-x :>
               <icon i=x title="${h:max}X chromosome ancestor — ${h:num} recombination${h:s}" style="${h:style}"/>
            <h:b-doubts :>
               <span style="${h:width}" class=ib :>
                  <icon i=blackq style="${h:tform}" alt='?'/>
               <a href="#${h:to}" class=ib style="${h:width}" :>
                  <icon i=blueq style="${h:tform}" alt='?'/>
               <a href="#${h:to}" class="ib fade2" style="${h:width}" :>
                  <icon i=blueq style="${h:tform}" alt="(?)"/>
            <h:b-supp :>
               <button data-toggle="${h:start}"
                     title="Toggle extra notes" class=plb :>
                  <icon i=plus/>
         <div class=indent:>
            <p class=bioline:><h:events/>
            <h:notes:><div class=notes:><h:content/>
            <h:suppnotes:><div id="supp${h:start}" class=supp:><h:content/>
            <h:issues:>
               <div class=kids :>
                  <u class=listhead:>Issue<h:cap/>
                  <h:items/>
            <h:sibs:>
               <div class=sibs :>
                  <u class=listhead:>Siblingry<h:cap/>
                  <h:items/>
<h:post/>
<div style='padding-bottom:30%':>&nbsp;
|]

