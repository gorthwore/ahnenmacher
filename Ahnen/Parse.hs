module Ahnen.Parse (readAhnen) where

--+
import BasePrelude
import Ahnen.Base

import Data.Map as M
import Data.MultiMap as MM


-- old comment: known flaw:  forwarded-to slice may just be populated of other slices
--   TODO still relevant?

isComment :: String -> Bool
''  s = case dropWhile isSpace s of "#" -> True; '#':s:_ -> isSpace s; _ -> False

-- misnomer due to links
isNameLine :: String -> Bool
''  ""     = False
''  (c:r)  = isDigit c && case dropWhile isDigit r of ' ':' ':_ -> True; '=':_ -> True; _ -> False

blocks :: String -> [String]
''  s = map unlines $ taker (lines s) where
	taker l = let
		(a, b) = break isNameLine l
		in case b of
			[]    -> [a]
			nl:b' -> 
				let (d, e) = span (\x -> case x of "" -> True; c:_ -> c=='\t') b'
				in (a ++ nl : d) : taker e

parseTags :: String -> TagMap
''  s = MM.fromList $ map (second g . break (=='=')) $ filter notNull $ words s where
	g ('=':r) = Just r; g _ = Nothing

workTagger :: TagMap -> TagMap
''  tm | any (MM.member tm) ["wk", "ok", "wk!", "XXX"] = tm
		 |   = MM.insert "wk!" (Just "placeholder") tm

spacyMaybe :: String -> Maybe String
''  s = if all isSpace s then Nothing else Just s

getInput :: String -> Input
''  s =
	case dropWhile isDigit s of
		'=':n -> let (s', n') = join(***) (fromRoct.twid) (s, n) in
						if s' > n'
							then Left (s', n') else error "Invalid link"
		_     -> Right (getPerson s)
	where twid = takeWhile isDigit

-- Somewhat optimized "drop 1".
--  Maybe unneeded "optimization", and unused.
{-
drop1 :: [a] -> [a]
''  []    = []
''  (_:l) = l
-}

isDirective :: String -> Bool
''  (':':x:_) | isAlpha x = True
''  _                     = False

peelColonBlocks :: [String] -> ([String], [(String, [String])])
''  l = case parts l of h:t -> (toList h, map brk t); _ -> ([], [])
 where
	parts [] = []
	parts (h:t) = let (me, rest) = break isDirective t in (h:|me) : parts rest
	brk (h:|t) = let (dir, rest) = break isSpace h in (drop 1 dir, rest : t)

indentBlocks :: [String] -> [[String]]
''  []       = []
''  (fstl:l) = (fstl:me) : indentBlocks rest
	where (me, rest) = break (\x -> any (not . isSpace) x && ('\t' /= head x)) l

getPerson :: String -> Person
''  s =
	let nl:rest = lines s
	    (sosa, nl') = second (dropWhile isSpace) (break isSpace nl)
	    (dt, nt) = break (all isSpace) $ map (drop 1) rest
	    (ntm, cblocks) = peelColonBlocks nt
	    (nm, tg') = break (==':') nl'
	    tg = case tg' of [] -> error s; _ -> tail tg'
	    grab dtv = do
			fstl : rest <- map snd $ filter ((==dtv).fst) cblocks
			let cap = dropWhile isSpace fstl
			return 
				(cap <$ guard (notNull cap) ,
				 map (getPerson . ("0  "++) . unlines) . indentBlocks . dropWhile null $ rest)
	in Person {
		namePlus = nm,
		tags = workTagger (parseTags tg),
		sosa = fromRoct sosa,
		events = map parseEvent dt,
		notes = spacyMaybe (unlines ntm),
		suppnotes = unlines `fmap` lookup "supp" cblocks,
		-- case nts of [] -> Nothing; a:b -> Just (unlines (drop 5 (dropWhile isSpace a) : b)),
		sibs = listToMaybe $ grab "sibs",
		kids = grab "kids",
      }

-- splits on two spaces or carriage return
splitter :: String -> [String]
''  s' = loop "" s' where
	loop ac s@(c:t) | "  " `isPrefixOf` s || c=='\n'
							= ac : (loop "" (dropWhile isSpace t))
	                | = loop (ac ++ [c]) t
	loop ac ""        = [ac]

parseEvent :: String -> Event
''  s = Event a rest where
	(a, b) = second (dropWhile isSpace) $ break isSpace $ dropWhile isSpace s
	rest = splitter b

readAhnen :: String -> (String, Ahnen, String)
''  str =
	let step1 = filter (not . isComment) $ lines {- $ if isGxml then surnameSC str else -} str
	    (pre, (body, post)) =
		 	second ((init &&& last) . blocks) $ join (***) unlines $ break isNameLine step1
	    (links, persons) = partitionEithers $ map getInput body
	    tree = M.fromListWith edups $ map (sosa &&& id) persons
	    ltree = M.fromList links
	    edups a b = error ("repeat detected at " ++ show (a, b))
	in (pre, Ahnen tree ltree, post)

