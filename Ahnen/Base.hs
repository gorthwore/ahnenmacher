module Ahnen.Base where

--+
import BasePrelude

import Data.Set (Set), as S
import Data.Map (Map), as M
import Data.MultiMap as MM
import Data.Time
import Data.ByteString.Builder
import Data.Text (Text)


--  Don't know if will keep this, should rename to Html tho.
data OutputType = Gxml | Plain deriving stock Eq
oType = Gxml
isGxml = oType == Gxml
outputAlt a b = if isGxml then a else b

type Sosa = Integer


half = flip div 2

log2 :: Integer -> Int
log2 n | n <= 1 = 0
       |        = 1 + log2 (half n)

pow2 :: Integral a => a -> Integer
''  n  = 2 ^ n

σ :: Ord a => [a] -> Set a
σ = S.fromList

notNull :: [a] -> Bool
''  = not . null

type Tag = (String, Maybe String) -- used only for parsing?
type TagMap = MM.MultiMap String (Maybe String)

instance (Show a, Show b, Ord b) => Show (MM.MultiMap a b) where
	show x = show $ MM.toMapOfSets x

data Event = Event !String ![String]
	deriving stock (Show, Eq)

data Person = Person {
	namePlus :: !String,
	tags :: !TagMap,
	sosa :: !Integer,
	events :: ![Event],
	notes :: !(Maybe String),
	suppnotes :: !(Maybe String),
	sibs :: !(Maybe (Maybe String, [Person])),
	kids :: ![(Maybe String, [Person])],
   } deriving stock (Show)


type People = Map Integer Person
data Entry = Bio !Sosa | SameAs !Range | Unknown !Interval
   deriving stock (Eq, Show)

data Ahnen = Ahnen { people :: People, linkMap :: LinkMap }
	deriving stock (Show)
-- data Ahnen' = Ahnen' { ahnen :: Ahnen, shown :: Set Integer }

data XRecomb = XRecomb { xRecombs :: !Int, isMaxX :: !Bool }
   deriving stock (Eq, Show)
type XInfo = Maybe XRecomb
data Haplogroup = Haplogroup { hgMain :: Text, hgExtra :: Maybe Text }
   deriving stock (Eq, Show)

instance Ord XRecomb where
   compare (XRecomb a m) (XRecomb b n) = compare (b, n) (a, m)

data DnaInfo = DnaInfo {
   , xInfo :: !XInfo
   , yHg :: !(Maybe Haplogroup)
   , mtHg :: !(Maybe Haplogroup)
   }

data AhnenPlus = AhnenPlus {
	, _ahnen :: Ahnen
   , _rootSex :: !Sex
	, _invLinks :: InvLinkMap
	, _doubts :: DoubtMap
	, _coverage :: Coverage
	, _firstgen :: !Int
   , _multis :: Map Sosa Integer
   , _allthrus :: [Sosa]
   , _entries :: [[Entry]]
   , _ilinks :: Set Sosa
   , _extras :: Dynamic
   , _dnaMap :: Map Sosa DnaInfo
	} -- deriving (Show) -- don't try to show, it's infinite

type Link = (Integer, Integer)
type Range = (Integer, Int)
type Interval = (Integer, Integer)

type LinkMap = Map Sosa Sosa
type InvLinkMap = Map Sosa [Sosa]

type DoubtList = [(Sosa, Float)]
data DoubtEntry = DoubtEntry { someDoubt :: !DoubtList, fullDoubt :: !DoubtList }
   deriving stock (Eq, Show)
type DoubtMap = Map Sosa DoubtEntry
noDoubt = DoubtEntry [] []

type Coverage = Map Sosa [Integer]

type Input = Either Link Person

data Sex = Male | Female deriving stock (Eq, Ord, Bounded, Enum, Show)


-- only works for sosa > 1
fromSex a b sex = case sex of Male -> a; Female -> b

sosaSex :: Integer -> Sex
''  sosa = if even sosa then Male else Female


entryStart :: Entry -> Sosa
''  (Unknown (x, _)) = x
''  (Bio x)          = x
''  (SameAs rg)      = fst $ rangeToInterval rg

ecatch :: Show a => String -> Either a b -> b
''  typ = either (error . (\e -> typ ++ ": " ++ e) . show) id

--  Like takeWhile, but takes one more ("inclusive").
takeWhile1 p = loop where loop (x:l) = x : if p x then loop l else []; loop [] = []

rangeToInterval :: Range -> (Integer, Integer)
''  (rt, gn) = (rt `shiftL` gn, (rt+1) `shiftL` gn - 1)


fromRoct :: String -> Sosa
''  = half . until odd half . fst . head . readOct . ('1':)

--   this is used a lot, and inefficient
toRoct :: Sosa -> String
''  n = head [ x | f <- [1,2,4], '1':x <- [showOct (f*(2*n+1)) ""] ]

--   I don't even know if this is any better
toRoctB :: Sosa -> Builder
''  n = go ((2*n+1) * case log2 n `mod` 3 of 0 -> 4; 1 -> 2; _ -> 1) where
   go x | x <= 1 = mempty
        | = go (x `shiftR` 3) <> (word8 $! fromIntegral (x .&. 7) + 48)
{-# NOINLINE[0] toRoctB #-} -- no idea why

-- A weird historical combinator, but finds first tag of given name with argument.
findTag :: String -> TagMap -> Maybe String
''  tag tm = case catMaybes $ MM.lookup tag tm of
	[] -> Nothing
	[x] -> Just x
	_   -> error $ tag ++ " tag can only appear once."


rootParam :: Ahnen -> String -> Maybe String
''  ah nm = findTag nm =<< tags <$> M.lookup 0 (people ah)

rootSosa :: Ahnen -> Sosa
''  ah = fromMaybe 1 $ fromRoct <$> rootParam ah "root"

rootSex :: Ahnen -> Sex
''  ah = case fromMaybe "m" $ rootParam ah "sex" of
	"m" -> Male; "f" -> Female; _ -> error "Invalid sex!"


covered :: Coverage -> Range -> Bool
''  cvg (!a, !b) = case a `M.lookup` cvg of Nothing -> False; Just x -> x !! b > 0


--  Given a set of "same as" forwards, compute where an individual at a
--     Sosa value actually is.
--  Somewhat inefficient... would like tail recursion. - tried, couldn't do it
--  This needs documentation, particularly as it's used weirdly with an empty
--  people map when finding parents.
forward :: People -> LinkMap -> Sosa -> Sosa
''  ppl mp = loop where
	loop n = let n' = loop' True n in if n==n' then n else loop n'
	loop' _  1 = 1
	loop' g0 n | g0 && n `M.member` ppl = n
			     | = M.findWithDefault (let (t, s) = n `divMod` 2 in s + 2 * loop' False t) n mp

--  Prob right way to do this.
forward' (Ahnen ppl mp) = forward ppl mp

forwardRange :: People -> LinkMap -> Range -> Range
''  ppl mp (n, g) = (n', g)
	where n' = forward (if g==0 then ppl else M.empty) mp n


--  Number showing.

-- show to two decimal places or two sig figs, whichever is more
-- still unsatisfactory as last digit rounds up too often
showPct :: Float -> String
'' v  = a ++ '.' : b ++ take 2 rds
	where
		vadj = if v==0 then 0 else 0.005 * 10 ^^ (0 `min` snd (floatToDigits 10 v))
		(a, b')  = span (/='.') (showFFloat Nothing (v+vadj) "0")
		(b, rds) = span (\x -> a=="0" && x=='0') (tail b')
		-- sd' = sd -- case rds `compare` "50" of GT -> sd1; EQ | odd (ord sd) -> sd1; _ -> sd
			-- where sd1 = succ sd

--  Display an integer.

---  prob not very efficient
showI :: Integer -> String
''  x | x < 10000 = s
		|           = reverse $ concat $ intersperse "," (unfoldr get3 (reverse s))
	where s = show x; get3 [] = Nothing; get3 x = Just $ splitAt 3 x

showIB = stringUtf8 . showI

dispRoct s = concat $ intersperse (if isGxml then "\8201" else " ") $ unfoldr take4 s where
	take4 [] = Nothing
	take4 l | length l <= 5 = Just (l, [])
	        |               = Just $ splitAt 4 l

dispRoctB :: Sosa -> Builder
''  n = go (2 - ((l2`div`3)+3)`mod`4) ((2*n+1) * case l2 `mod` 3 of 0 -> 4; 1 -> 2; _ -> 1) where
   l2 = log2 n
   go m x | x <= 1 = mempty
          | = go ((m+1)`mod`4) (x `shiftR` 3)
               <> (inter $ m==3 && x>=16)
               <> (word8 $! fromIntegral (x .&. 7) + 48)
   inter b = if b then "\8201" else mempty
{-# NOINLINE[0] dispRoctB #-}

showInterval :: (Integer, Integer) -> String
''  (a, b) | a==b  = showR a
           |       = showR a ++ (if isGxml then "&ndash;" else " - ") ++ showR b
	where showR = dispRoct . toRoct

showIntervalB :: (Integer, Integer) -> Builder
''  (a, b) | a==b  = dispRoctB a
           |       = dispRoctB a <> "\8211" <> dispRoctB b

{-
wholeGen :: Int -> (Integer, Integer)
'' g  = rangeToInterval (1, g)
-}

showRange :: Range -> String
''  = showInterval . rangeToInterval

showRangeB :: Range -> Builder
''  = showIntervalB . rangeToInterval


--  move elsewhere?
scoreSpace = map (\c -> if c=='_' then ' ' else c)


-- logging stuff

note :: String -> IO ()
note x = do
	now <- getCurrentTime
	hPutStrLn stderr $ take 26 (formatTime undefined "%F %T.%q" now) ++ "  " ++ x

traceLogM :: Monad μ => String -> μ ()
''  x =
   let {-# NOINLINE now #-}
       now = x `seq` unsafePerformIO getCurrentTime
   in traceM $ take 26 (formatTime undefined "%F %T.%q" now) ++ "  " ++ x

hiddenPerson :: Sosa -> Person
''  sosa = Person {
	namePlus = "Hidden", tags = MM.empty, sosa = sosa, events = mempty,
		notes = mempty, suppnotes = mempty, sibs = mempty, kids = mempty }

