module Ahnen.Utils where

--+
import BasePrelude
import Data.Map as M
import Data.ByteString.Builder

import Ahnen.Base


--  A new module meant to collect functions between Base and Ahnen.
--  (Slowly migrate as appropriate.)


--  Find all Sosas that forward to me using inverse map.
unforwards :: InvLinkMap -> Sosa -> [Sosa]
''  mp n = loop n where
	loop 1 = []
	loop n = M.findWithDefault [] n mp
               ++ let (t, s) = n `divMod` 2 in map ((s+).(2*)) (loop t)


type Tagger = Int -> Maybe Builder
rootGentag :: Ahnen -> Tagger
''  ah =
   case rootParam ah "gentag" of
      Just sp@(rt:_) ->
         if | isDigit rt ->
               \g -> let x = read sp - g in guard (x >= 1) *> pure (intDec x)
            | isAsciiUpper rt ->
               \g -> let x = fromEnum rt - g
                     in guard (x >= fromEnum 'A') *> pure (charUtf8 $ toEnum x)
            | -> error $ "unsupported gentag " <> sp
      _ -> const Nothing

