module Ahnen.Html (hx, hxm, hxm_p, surnameSC) where

--+
import BasePrelude
import Ahnen.Base (σ)

import Text.XmlHtml
import Data.ByteString.Lazy as BL
import Data.ByteString.Builder
import Data.Default

import Text.Hxml
import Text.Minimark (minimarkNodesWith, setMark)

-- import Language.Haskell.TH.Quote
-- import Language.Haskell.TH.Syntax (liftData)

runMinimark :: [Node] -> [Node]
''  = minimarkNodesWith (setMark '/' (Just "slash") def)


deriving stock instance Data Node

--  Functions for parsing, converting, quasiquoting.

sh :: Show a => Either a b -> b
sh = either (error . show) id

stringToLbs :: String -> BL.ByteString
''  = toLazyByteString . stringUtf8

fromHxml :: BL.ByteString -> BL.ByteString
''  = sh . parseHxml

fromHtml :: BL.ByteString -> [Node]
''  = docContent . sh . parseHTML "" . BL.toStrict

-- minimarkNodes $ docContent doc'

{-
-- not currently using a quasiquoter
noQQ = QuasiQuoter {
	quotePat  = cant "a pattern",
	quoteExp  = cant "an expresson",
	quoteType = cant "a type",   
	quoteDec  = cant "a declaration",
	} where cant = error . (\x -> "Cannot be used as " <> x <> ".")
-}


--  Old code.  Currently handled by Minimark, so unused.
--  Keeping as have reservations about current approach.
surnameSC :: String -> String
''  ""       = ""
''  (c:'/':s) | c `elem` σ" \n\t("  =
		c : let (n, r) = break (=='/') s in
			case r of
				_:r' | not (null n) && not (isSpace (head n))
                     && not (isSpace (last n)) && all isSurnameChar n
					  -> "<span class=sc>" ++ n ++ "</span>" ++ surnameSC r'
				_    -> '/' : surnameSC s
''  (c:s)    = c : surnameSC s

--  Entities count as surname chars.
isSurnameChar :: Char -> Bool
''  c = isAlpha c || c `elem` σ" -&;_"


-- TODO do i need any of this?
{-
htm = noQQ { quoteExp = liftData . fromHtml . stringToLbs }
htm' = fromHtml . stringToLbs
-}

hx, hxm {- , hxm' -} :: String -> [Node]
hx = fromHtml . fromHxml . stringToLbs
hxm = runMinimark . fromHtml . fromHxml . stringToLbs
-- hxm' = runMinimark . fromHtml . fromHxml . stringToLbs . dropWhile (=='\n')

hxm_p = runMinimark . (TextNode "\n\n\8203" :) . fromHtml . fromHxml . stringToLbs

