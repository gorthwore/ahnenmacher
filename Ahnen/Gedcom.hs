module Ahnen.Gedcom (ahnenToGedcom) where

--+
import BasePrelude
import Ahnen.Base
import Ahnen.Ahnen
import Data.Map as M
import Data.Set as S
import Data.List.Split
import Fmt


--  TODOs:
--    places? = skip for now
--     same as handling = skip for now
--    forwarding

---  llines at least won't allow alt surnames
stripThirdSlash :: String -> String
''  = loop (0 :: Int) where
	loop i ('/':s) | i<2 = '/' : loop (i+1) s
	               |     = loop i s
	''   i (c:s)         = c : loop i s
	''   _ _             = []

personToGedcom :: Person -> String
''  Person { .. } =
      "0 @I"+|sosa|+"@ INDI\n"
      <> "1 NAME "+|(stripThirdSlash $ intercalate " OR " $ splitOn " / " namePlus)|+"\n"
      +| chunk "BIRT" ["b", "bp"] |++| chunk "DEAT" ["d", "bd"] |+"" where
	findEvent ev = listToMaybe [ dat | Event ev' dat <- events, ev == ev' ]
	getYear dt = case words dt of
		yr':_ -> do
			guard $ length (filter isDigit yr') >= 4
			pure yr'
		_ -> Nothing
	mbBlock :: String -> Maybe String -> String
	mbBlock titl = maybe "" (\x -> "1 " ++ titl ++ "\n2 DATE " ++ x ++ "\n")
	--  prob can do better than this mess
	chunk titl evs = mbBlock titl $ listToMaybe $ catMaybes $ map getYear $ fromMaybe [] $ foldMap findEvent evs

--  Incorporates whole ahnen for sex
personToGedcom' :: Ahnen -> M.Map Sosa String -> Person -> String
''  ahn fams pers = concat $
		personToGedcom pers :
		("1 SEX "+|sex|+"\n") :
		[ "1 FAMS @F"+|kid|+"@\n" | let kid = half s, kid `M.member` fams ] ++
		[ "1 FAMC @F"+|s|+"@\n" | s `M.member` fams ] ++
		[]
	where
		sex = take 1 $ show $ if s==1 then rootSex ahn else sosaSex s
		s = sosa pers

personToFamGedcom :: Ahnen -> Person -> Maybe String
''  ahn pers = do
	let parents = [ "1 "+|who|+" @I"+|p|+"@\n"
			| (who, p) <- [("HUSB" :: String, 2*s), ("WIFE", 2*s+1)], p `M.member` people ahn ]
	guard $ length parents > 0
	pure $ concat $ [ "0 @F"+|s|+"@ FAM\n1 CHIL @I"+|s|+"@\n" ] ++ parents
	where s = sosa pers

ahnenToGedcom :: Ahnen -> String
''  ahn@(Ahnen { people }) = do
	let ahn' = stuffAhnen ahn
	let doubted = S.fromList $ M.keys $ M.filter (/=noDoubt) $ _doubts ahn'
	let people' =
			M.withoutKeys (M.delete 0 people) doubted
				`M.union` M.fromList [ (i, hiddenPerson i) | i <- [1 .. 2^_firstgen ahn' - 1] ]
	let fams = M.mapMaybe (personToFamGedcom ahn { people = people' }) people'
	let indis = M.map (personToGedcom' ahn fams) people'
	"0 HEAD\n" ++
		concat (M.elems indis ++ M.elems fams) ++
		"0 TRLR\n"

