module Ahnen.Ahnen where
-- (checkAhnen, stuffAhnen)

--  General Ahnen utilities.

--+
import BasePrelude

import Ahnen.Base
import Data.Map (Map), as M
import Data.MultiMap as MM
import Data.Set (Set), as S
import Data.Text (Text), as T


someKnown :: [Entry] -> Bool
''  = any (\case Unknown _ -> False; _ -> True)

ahnenToEntries :: Ahnen -> Coverage -> Range -> [Entry]
''  (Ahnen { .. }) cvg = mergeu . loop where
   shrink :: Integer -> Int -> [Entry]
   shrink rt gn = case () of
      () | gn==0 -> done
         | ncv 0 -> unk 0 ++ onw 1
         | ncv 1 -> onw 0 ++ unk 1
         |       -> done
      where
         --  Had to put M.empty in here to avoid error... look into XXX
         ncv x = not $ covered cvg $ forwardRange M.empty linkMap (rt*2+x, gn-1)
         unk x = [ unkr (rt*2+x, gn-1) ]
         onw x = shrink (rt*2+x) (gn-1)
         done  = [ SameAs (rt, gn) ]
   loop (rt, gn) =
      case (covered cvg (rt', gn), gn==0, rt==rt') of
         (False, _,   _) -> [ unkr (rt, gn) ]
         (_, True, _) | rt `M.member` people -> [ Bio rt ]
         (_, _, False)   -> shrink rt gn
         _               -> loop (rt*2, gn-1) ++ loop (rt*2+1, gn-1)
      where rt' = forward (if gn==0 then people else M.empty) linkMap rt
   unkr = Unknown . rangeToInterval
   mergeu []    = []
   mergeu (Unknown (a, _) : Unknown (_, c) : l) = mergeu (Unknown (a, c) : l)
   mergeu (x:l) = x : mergeu l

{-
ahnenToGens :: AhnenPlus -> [(Int, [Item])]
''  ahn' = takeWhile1 (someKnown . snd)
      [(g, ahnenToItems ahn cvg (onlyOn, g-onlyDep)) | g <- [fgen..]]
   where
      ahn = _ahnen ahn'; cvg = _coverage ahn'; fgen = _firstgen ahn'
      onlyOn = rootSosa ahn
      onlyDep = log2 onlyOn
-}


-- we guess X value based on Sosa and root sex
-- (and not knowing if multiple descents)
-- (not happy with this algorithm, seems way too messy)
xLevel :: Sex -> Sosa -> XInfo
''  sex n = go (sosaSex n) (half n) where
   start = XRecomb 0 True
   go _    0 = Just start
   go Male 1 = guard (sex == Female) $> start
   go Female 1 | sex == Male = Just $ XRecomb 1 True
   go sx n = case sx of
      Male -> guard (odd n) *> bump 0 True next
      _    -> bump 1 (even n) next
      where next = go (sosaSex n) (half n)
   bump jp ism = fmap \ (XRecomb ct mx) -> XRecomb (ct + jp) (mx && ism)

-- no longer used?  mark for termination
fetchDNA :: People -> Sosa -> Sex -> Maybe String
''  ppl sosa sex = head do
   let lastP = until (\x -> x == 1 || sosaSex x /= sex) half sosa
   p <- iterate (\x -> x*2+fromIntegral (fromEnum sex)) lastP
   tag <- maybeToList $ tags `liftM` (p `M.lookup` ppl)
   return $ fromSex "-y" "-mt" sex `findTag` tag

--  Checks if an interval is (inclusive) at all in the map.
meetRange :: Ord a => (a, a) -> Map a b -> Bool
''  (a, b) mp = maybe False ((<=b) . fst) (M.lookupGE a mp)


--  Ahnen derived data.

stuffAhnen :: Ahnen -> AhnenPlus
''  ahn = ahp where
	ahp = AhnenPlus {
      _ahnen    = ahn,
      _rootSex  = rootSex ahn,
      _doubts   = doubtMap ahp,
      _invLinks = invertMap (linkMap ahn),
      _coverage = coverage ahn,
      _firstgen = head [  -- TODO empty ahnen?
         g | g <- [onlyDep ..], meetRange (rangeToInterval (onlyOn, g-onlyDep)) $ people ahn ],
      _multis = multisMap ahp,
      _allthrus = allthrus $ _coverage ahp,
      -- _ilinks   = S.fromList $ map il $ concatMap snd tableau,
      _entries  = takeWhile1 someKnown [
         ahnenToEntries ahn (_coverage ahp) (onlyOn, g')
            | g' <- [_firstgen ahp - onlyDep ..] ],
      _ilinks   = S.fromList $ map entryStart $ concat $ _entries ahp,
      _extras   = toDyn (),
      _dnaMap   = mkDnaMap ahp,
		}
	onlyOn = rootSosa ahn
	onlyDep = log2 onlyOn

--  TODO:  Needs to address no kid but still linked to (odd case to be sure).
multisMap :: AhnenPlus -> Map Integer Integer
''   ahp@(AhnenPlus { _ahnen = Ahnen { people } }) = multis where
   multis = M.mapWithKey summer people
   summer n _
      | n>1 && half n `M.member` people =
         sum $ mapMaybe (flip M.lookup multis) $ halfsies ahp n
      | = 1

--  List of all children, including via links.
--   case of root is a little touch and go
halfsies :: AhnenPlus -> Integer -> [Integer]
-- ''  _ 1 = []
''  AhnenPlus { _invLinks = il, _ahnen = ah } n =
   half n :
      filter exists (inv $ half n) ++ map half (filter (not . exists) $ inv n)
   where
      exists k = M.member k (people ah)
      inv k = M.findWithDefault [] k il

doubtMap :: AhnenPlus -> DoubtMap
''  ahp@AhnenPlus { .. } = doubts where
   doubts = M.mapWithKey doubter ppl
   ppl = people _ahnen
   f n l = map (\x -> (n, read x :: Float)) $ catMaybes l
   doubter 0 _ = noDoubt -- when would this arise? TODO
   doubter n v =
      let (ls, la) = unzip do
            flip map (halfsies ahp n) \x ->
               let DoubtEntry s1 a1 = M.findWithDefault noDoubt x doubts
               in (s1, a1 ++ f x (fromMaybe [] (MM.lookup "-q" <$> tags <$> x `M.lookup` ppl)))
          ila = foldl1' intersect la
      in DoubtEntry {
         , someDoubt = foldl1' union ls ++ (foldl1' union la \\ ila)
         , fullDoubt = ila ++ f n (MM.lookup "-q!" (tags v))
         }

mkDnaMap :: AhnenPlus -> Map Integer DnaInfo
''  ahp = ans where
   ans = M.mapWithKey go $ people ahn
   ahn = _ahnen ahp
   start = 2 ^ (_firstgen ahp + 1)
   get tag p = fmap (brk . T.break (=='_') . T.pack) $ findTag tag $ tags p where
      brk (a, b) = Haplogroup a
                     $ if T.null b
                        then Nothing
                        else Just $ T.replace "_" " " $ T.tail b
   go n p =
      if n < start
         then DnaInfo byRootX getY getMt
         else
            let kids = halfsies ahp n
                dnas = map (ans M.!) kids
            in DnaInfo {
               , xInfo = maximum $ Nothing : map deriveX kids
               , yHg = find $ getY : (guard (even n) *> map yHg dnas)
               , mtHg = find $ getMt : (guard (odd n) *> map mtHg dnas)
               }
      where
      getY = get "-y" p
      getMt = get "-mt" p
      find = listToMaybe . catMaybes
      byRootX = xLevel (_rootSex ahp) n
      deriveX k | even n /= even k = bump (fromEnum $ odd n) True
                | even n           = Nothing
                |                  = bump 1 False
         where
         bump jp ism = (xInfo $ ans M.! k) <&>
            \ (XRecomb ct mx) -> XRecomb (ct + jp) (mx && ism)

--  Haskell's built-in map functions still not adequate.
--   TODO how about years later now?
invertMap :: Ord b => Map a b -> Map b [a]
''  mp = M.fromListWith (++) $ map swap' $ M.toList mp
	where swap' (k, v) = (v, [k])

coverage :: Ahnen -> Coverage
''  (Ahnen { .. }) = mem where
   mem = M.fromList [ (rt, 1 : map (cnt rt) [1..])
                     | rt <- [1 .. freebies] ++ M.keys people ]
   freebies = maybe 1 (\ (k, _) -> pow2 (log2 k) - 1) $ M.lookupGT 0 people
   cnt rt gn =
		case rt `M.lookup` linkMap of
			Nothing -> get 0 + get 1
			Just to -> mem M.! to !! gn
		where get x = maybe 0 (!! (gn-1)) (forward people linkMap (rt*2+x) `M.lookup` mem)

--  TODO formerly this had an allthru list for every node
--   not maintaining that as of now due to current algorithm
-- this function badly needs cleaning up!!
allthrus :: Coverage -> [Sosa]
''  cvg = loop 0 rules 1 where
   loop _ []          sosa = repeat sosa
   loop g ((s, g'):l) sosa = replicate (g' - g) sosa ++ loop g' l s
   uniqmax :: forall a. [(a, Int)] -> Maybe (a, Int)
   ''  = loop (0, 0, Nothing) where
      loop (mx, mx2, mb) ((n, v) : l)
         | v > mx  = c (v, mx, Just n)
         | v == mx = c (v, mx2, Nothing)
         | v > mx2 = c (mx, v, mb)
         |         = c (mx, mx2, mb)
         where c = flip loop l
      loop (_, mx2, mb) _ = {- guard (mx2>0) *> -} ((,mx2) <$> mb)
   post ((_, a) : (s', b) : l) | a == b
               = post $ (s', b) : l
   ''   (x:l)  = x : post l
   ''   []     = []
   rules = post $ catMaybes do
      (g, e) <- zip [0..] $ takeWhile (< fst (M.findMax cvg)) $ iterate (2*) 1
      -- bad way to do this i'm sure
      pure $ fmap (\ (s, v) -> (s, g+v)) $ uniqmax
         [ (sosa, length . takeWhile (/=0) $ val)
         | (sosa, val) <- takeWhile ((<2*e).fst)
                           $ dropWhile ((<e).fst)
                           $ M.toAscList cvg ]


--  Validity checking.

checkAhnen :: Ahnen -> ()
''  Ahnen { .. } =
	and [ M.findWithDefault (error $ ("no child "++) $ toRoct s) (s`div`2) people `seq` True
			| s <- M.keys people, s >= minimum (filter (>0) $ M.keys people) * 2 ]
		`seq`
	and [ M.findWithDefault (error $ ("bad link "++) $ toRoct s) (half s) people `seq` True
			| s <- M.keys linkMap ]
		`seq`
	()


--  Random functions to compute derived info from Ahnen.
--  (These aren't used, but I can invoke them to get kinds of data.)
--   TODO put under CLI


--  Utility functions for below.

closure :: Ord a => (a -> a) -> Set a -> Set a
''  f s =
	let s' = s `S.union` S.map f s in
	if S.size s == S.size s' then s else closure f s'

-- we conventionally excise 0
descClosure :: Ahnen -> Set Sosa
''  ah = closure (\x -> if x==1 then 1 else half x) (S.delete 0 $ M.keysSet $ people ah)


longLinesMap :: Ahnen -> (Sosa -> Sosa) -> Map Sosa (Int, [Sosa])
''  ah rel = foldr M.delete tbl (map rel $ M.keys tbl) where
	tbl = flip M.fromSet (descClosure ah) $ \s ->
		case forward' ah (rel s) `M.lookup` tbl of
			Just (x, l) -> (x + 1, s:l)
			_           -> (1, [s])

-- doesn't seme to work
coverScoreMap :: Ahnen -> Map Sosa Rational
''  ah = tbl where
	tbl = flip M.fromSet (descClosure ah) $ \s ->
		let f x = fromMaybe 0 (forward' ah x `M.lookup` tbl)
		in 1 + (f (2*s) + f (2*s+1)) / 2

raceMap :: Ahnen -> Map Sosa (Map Text Rational)
''  ah = tbl where
   tbl = flip M.fromSet (descClosure ah) $ \s ->
      let f x = fromMaybe (M.singleton "u" 1) (forward' ah x `M.lookup` tbl) in
         case s `M.lookup` people ah of
            Just pers | Just v <- findTag "-race" $ tags pers
               -> M.singleton (T.pack v) 1
            _  -> (/2) <$> M.unionWith (+) (f $ 2*s) (f $ 2*s+1)

dumpRace :: Map Text Rational -> String
''  rmap = unlines do
   (k, v) <- sortOn (Down . snd) $ M.assocs rmap
   pure $ printf "%14.9F" (fromRational (v*100) :: Double) ++ " %   " ++ T.unpack k


--  Misc unused utility functions.

mother, father :: Sosa -> Sosa
mother x = 2*x + 1; father x = 2*x
legate rsex x = 2*x + (if odd x && (x/=1 || rsex==Female) then 0 else 1)

--  No longer used now that countMultis was rewritten, and then dropped.
isAncestorOf :: Integer -> Integer -> Bool
''  s1 s2 = s1 `shiftR` (log2 s1 - log2 s2) == s2

downLine sosa = takeWhile (>0) $ iterate half sosa

halfR :: Integer -> Range -> Range
''  x (rt, gn) = (rt*2+x, gn-1)

